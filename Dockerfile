# escape=`

# Copyright (C) Microsoft Corporation. All rights reserved.
# Licensed under the MIT license. See LICENSE.txt in the project root for license information.

ARG WINDOWS_VERSION=1809
ARG FROM_IMAGE=mcr.microsoft.com/windows/servercore:${WINDOWS_VERSION}-amd64
FROM ${FROM_IMAGE}

SHELL ["powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]
RUN Get-PSDrive C

# # Reset the shell.
# SHELL ["cmd", "/S", "/C"]
# 
# # Set up environment to collect install errors.
# COPY Install.cmd C:\TEMP\
# ADD https://aka.ms/vscollect.exe C:\TEMP\collect.exe
# 
# # Download channel for fixed install.
# ARG VS_VERSION=15
# ARG CHANNEL_URL=https://aka.ms/vs/${VS_VERSION}/release/channel
# ADD ${CHANNEL_URL} C:\TEMP\VisualStudio.chman
# 
# # Download and install Build Tools for Visual Studio 2017 for native desktop workload.
# ADD https://aka.ms/vs/${VS_VERSION}/release/vs_buildtools.exe C:\TEMP\vs_buildtools.exe
# RUN C:\TEMP\Install.cmd C:\TEMP\vs_buildtools.exe --quiet --wait --norestart --nocache `
#     --channelUri C:\TEMP\VisualStudio.chman `
#     --installChannelUri C:\TEMP\VisualStudio.chman `
#     --add Microsoft.VisualStudio.Workload.VCTools --includeRecommended`
#     --installPath C:\BuildTools
# 
# # Use developer command prompt and start PowerShell if no other command specified.
# ENTRYPOINT C:\BuildTools\Common7\Tools\VsDevCmd.bat &&
# CMD ["powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]
